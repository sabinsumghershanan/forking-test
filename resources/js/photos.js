$(document).ready(function() {
    'use strict';
    
    function photo_markup_function (photo) {
      return '<div class="photo" style="float:left; padding:4px;">  <img src="' + photo.url+ '" height="60" width="60"/></div>';
    }
    
    $.ajax({
      url: 'https://jsonplaceholder.typicode.com/photos',
      dataType: 'json',
    }).done(function(data)  {
        data = $(data).slice(0,16);
        let photo_markup = '';
        $.each(data, function( index, photo ) {
          photo_markup += photo_markup_function(photo);
        });

        $('.js-photo-list').append(photo_markup);
      
    })
});