var user = {};
var friends = ['friends (1)','friends (3)','friends (2)','friends (5)','friends (4)','friends (7)','friends (6)','friends (9)','friends (8)','friends (11)'];

function updateExpressions() {
    var elements = document.querySelectorAll("*");
    for(var i=0;i<elements.length;i++) {
        if(elements[i].nodeName == 'SPAN' && elements[i].getAttribute("data-modaltxt") != undefined && user[elements[i].getAttribute("data-modaltxt")] !=undefined) {
            elements[i].innerText = user[elements[i].getAttribute("data-modaltxt")]; 
        }
        else if(elements[i].nodeName == 'IMG' && elements[i].getAttribute("data-modalsrc") != undefined && user[elements[i].getAttribute("data-modalsrc")] != undefined) {
            elements[i].src= user[elements[i].getAttribute("data-modalsrc")];
        }
    }
}

function loadURL(url,callback) {
    var xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function() {
       if (this.readyState == 4 && this.status == 200) {
           callback(this.responseText);
       }
    }
    xhttp.open("GET", url, true);
    xhttp.send();
}

var loadUserById = function(id) {
    loadURL("http://jsonplaceholder.typicode.com/users?id="+id,handleUser);
}

var handleUser = function(data) {
    user = JSON.parse(data)[0];
    user.friends = friends;
    user.city = user.address.city;
    user.coverphoto = "images/User1Cover.png";
    loadURL("https://jsonplaceholder.typicode.com/photos?albumId="+user.id,function(data) {
        user.photos = JSON.parse(data);
        user.thumbnailUrl = user.photos[0].thumbnailUrl;
        updateExpressions();
    });
}
function addContent(divName, content) {
     document.getElementById(divName).innerHTML = content;
}
window.onload = function() {
    loadUserById(2);
};