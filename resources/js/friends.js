$(document).ready(function() {
    'use strict';

function friends_markup_function (friends) {
    return '<div class="friends" style="float:left; padding:4px;">  <img src="' + friends.url+ '" height="60" width="60"/></div>';
  }
  
  $.ajax({
    url: 'https://jsonplaceholder.typicode.com/photos',
    dataType: 'json',
  }).done(function(data)  {
      data = $(data).slice(0,16);
      let friends_markup = '';
      $.each(data, function( index, friends ) {
        friends_markup += friends_markup_function(friends);
      });

      $('.js-friends-list').append(friends_markup);
    
  })
});